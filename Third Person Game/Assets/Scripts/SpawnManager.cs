﻿using UnityEngine;
using System.Collections;

public class SpawnManager : MonoBehaviour
{
	public GameObject[] enemies;
	public Transform[] spawnPoints;

	void Start ()
	{
		SpawnEnemy ();
	}
		
	void SpawnEnemy ()
	{
		int n = 0;
		foreach (Transform spawnPoint in spawnPoints) 
		{
			GameObject Enemy;
			int randEnemy = Random.Range (0, 2);
			Enemy = Instantiate (enemies [randEnemy], spawnPoint.transform.position, Quaternion.identity) as GameObject;
			Enemy.transform.parent = spawnPoint.transform;
			Enemy.name = "EnemyGroup0" + n;
			n++;
		}
	}
}
