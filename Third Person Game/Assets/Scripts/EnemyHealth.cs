﻿using UnityEngine;
using System.Collections;

public class EnemyHealth : MonoBehaviour 
{
	public int startingHealth = 100;
	public int currentHealth;


	// Use this for initialization
	void Start () 
	{
		currentHealth = startingHealth;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (currentHealth <= 0) 
		{
			Destroy (gameObject);
		}
	}

	public void TakeDamage(int amount)
	{
		currentHealth -= amount;
	}
}
