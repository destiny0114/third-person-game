﻿using UnityEngine;
using System.Collections;

public class ComboSystem : MonoBehaviour
{

	private Animator anim;
	private AnimatorStateInfo stateinfo;

	private const string Idlestate = "Grounded";
	private const string state1 = "Attack01";
	private const string state2 = "Attack02";
	private const string state3 = "Attack03";
	private const string state4 = "Attack04";

	private float HitCount = 0;


	private KeyCombo Combo1 = new KeyCombo (new KeyCode[] { KeyCode.B });
	private KeyCombo Combo2 = new KeyCombo (new KeyCode[] { KeyCode.B });
	private KeyCombo Combo3 = new KeyCombo (new KeyCode[] { KeyCode.B });
	private KeyCombo Combo4 = new KeyCombo (new KeyCode[] { KeyCode.B });
	private KeyCombo Combo5 = new KeyCombo (new KeyCode[] { KeyCode.B });

	public int AttackDamage = 20;
	public Collider[] attackHitboxes;

	void Start ()
	{
		anim = GetComponent<Animator> ();
		HitCount = 0;
	}


	void Update ()
	{
		stateinfo = anim.GetCurrentAnimatorStateInfo (0);

		if (!stateinfo.IsName (Idlestate) && stateinfo.normalizedTime > 1.0f) 
		{
			anim.SetInteger ("State", 0);
			HitCount = 0;
		}

		Attack ();
	}

	void Attack()
	{
		if (Combo1.Check() && stateinfo.IsName (Idlestate) && HitCount == 0 && stateinfo.normalizedTime > 0.1f) 
		{
			LaunchAttack (attackHitboxes [3]);
			anim.SetInteger ("State", 1);
			HitCount = 1;
		}

		else if (Combo2.Check() && stateinfo.IsName (state1) && HitCount == 1 && stateinfo.normalizedTime > 0.2f) 
		{
			LaunchAttack (attackHitboxes [2]);
			anim.SetInteger ("State", 2);
			HitCount = 2;
		}

		else if (Combo3.Check() && stateinfo.IsName (state2) && HitCount == 2 && stateinfo.normalizedTime > 0.35f) 
		{
			LaunchAttack (attackHitboxes [2]);
			anim.SetInteger ("State", 3);
			HitCount = 3;
		}

		else if (Combo4.Check() && stateinfo.IsName (state3) && HitCount == 3 && stateinfo.normalizedTime > 0.4f) 
		{
			anim.SetInteger ("State", 4);
			HitCount = 4;
		}
	}

	private void LaunchAttack (Collider col)
	{
		Collider[] cols = Physics.OverlapBox (col.bounds.center, col.bounds.extents, col.transform.rotation, LayerMask.GetMask ("Hitbox"));
		foreach (Collider c in cols) {
			EnemyHealth enemyHealth = c.GetComponent<EnemyHealth> ();

			if (enemyHealth != null) {
				enemyHealth.TakeDamage (AttackDamage);
			}
		}
	}
}
