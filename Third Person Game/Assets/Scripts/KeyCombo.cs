﻿using UnityEngine;
using System.Collections;

public class KeyCombo : MonoBehaviour {

	public KeyCode[] Keys;
	private int currentIndex = 0; //moves along the array as buttons are pressed

	public float allowedTimeBetweenButtons = 0.3f; //tweak as needed
	private float timeLastButtonPressed;

	public KeyCombo(KeyCode[] k)
	{
		Keys = k;
	}

	//usage: call this once a frame. when the combo has been completed, it will return true
	public bool Check()
	{
		if (Time.time > timeLastButtonPressed + allowedTimeBetweenButtons) currentIndex = 0;
		{
			if (currentIndex < Keys.Length)
			{
				if (Input.GetKeyDown(Keys[currentIndex]))
				{
					timeLastButtonPressed = Time.time;
					currentIndex++;
				}

				if (currentIndex >= Keys.Length)
				{
					currentIndex = 0;
					return true;
				}
				else return false;
			}
		}

		return false;
	}
}
