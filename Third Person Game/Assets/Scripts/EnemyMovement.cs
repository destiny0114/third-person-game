﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour 
{

	NavMeshAgent nav;
	Transform player;
	EnemyHealth enemyHealth;

	// Use this for initialization
	void Start () 
	{
		nav = GetComponent<NavMeshAgent> ();
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		enemyHealth = GetComponent<EnemyHealth> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (enemyHealth.currentHealth > 0) 
		{
			nav.SetDestination (player.position);
		} 

		else
			nav.enabled = false;
	}
}
